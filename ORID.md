# ORID

## O

This morning started with a code review, reviewing last week's parking-lot project. Then there was a group share, introducing the strategy pattern, observer pattern, and command pattern.

- Strategy pattern is a behavioral design pattern that allows the behavior of an algorithm to be selected at runtime, encapsulating the algorithms into separate classes and making them interchangeable without affecting the client code.
- Observer pattern is a behavioral design pattern that defines a one-to-many dependency relationship, when the state of an object changes, all of its dependents are notified and automatically updated. It is applicable to publish-subscribe patterns such as message queues, event buses, etc.
- The command pattern is a behavioral design pattern that encapsulates a request into an object, making it possible to parameterize different requests, queues, or log records, and supporting undoable operations.

## R

funny

## I

Among other things, the completion of this parking-lot project has improved my tdd thinking and ability to tasking decomposition of requirements. NoAvailableException will be used in it, and using exceptions will also deepen my understanding of the exception system in Java. At the same time, every time I started writing code, I would use a for loop to traverse the elements, and now I gradually started using stream api to traverse the array. Now I'm starting to use stream api to traverse arrays, and I'm realizing the convenience of stream api.

## D

I hope that the design patterns I learned today can be used in future projects, so that if a project meets the design requirements, I can use the design patterns to modify it. At the same time, I hope to deepen my understanding and use of stream api, so that I can skillfully utilize it in future projects.