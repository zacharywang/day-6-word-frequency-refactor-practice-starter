import java.util.*;
import java.util.stream.Collectors;

public class WordFrequencyGame {
    public static final String SPACE_PATTERN = "\\s+";
    public static final String CALCULATE_ERROR = "Calculate Error";
    public static final String SPACE = " ";

    public String getResult(String sentence) {
        try {
            List<Word> wordList = splitSentence(sentence);

            Map<String, List<Word>> wordMap = wordList
                    .stream()
                    .collect(Collectors.groupingBy(Word::getValue, HashMap::new, Collectors.toList()));

            wordList = sortWordsByFrequencyDesc(wordMap);

            return generateWordFrequencyPrintInfo(wordList);
        } catch (Exception e) {
            return CALCULATE_ERROR;
        }

    }

    private static List<Word> splitSentence(String sentence) {
        return Arrays
                .stream(sentence.split(SPACE_PATTERN))
                .map(word -> new Word(word, 1))
                .collect(Collectors.toList());
    }

    private static String generateWordFrequencyPrintInfo(List<Word> wordList) {
        return wordList.stream()
                .map(word -> word.getValue() + SPACE + word.getWordCount())
                .collect(Collectors.joining("\n"));
    }

    private static List<Word> sortWordsByFrequencyDesc(Map<String, List<Word>> wordMap) {
        return wordMap.entrySet().stream()
                .map(entry -> new Word(entry.getKey(), entry.getValue().size()))
                .sorted((previousWord, currentWord) -> currentWord.getWordCount() - previousWord.getWordCount())
                .collect(Collectors.toList());
    }
}
